import React, { useCallback, useState } from "react";
import "./phoneStore.css";
import { phoneData } from "./phoneData";
import ProductList from "./ProductList";
import ProductInfo from "./ProductInfo";

export default function PhoneStore() {
  const [phoneList, setPhoneList] = useState(phoneData);
  const [phoneInfo, setPhoneInfo] = useState({ ...phoneList[0] });

  const handleGetPhoneInfo = useCallback(
    (maSP) => {
      let clonePhoneList = [...phoneList];
      let index = clonePhoneList.findIndex((item) => {
        return item.maSP == maSP;
      });
      let currentPhone = clonePhoneList[index];

      setPhoneInfo({ ...currentPhone });
    },
    [phoneInfo]
  );

  return (
    <div>
      <div className="container py-3">
        <h1 className="text-success display-3">PHONE STORE</h1>
        <div className="my-5">
          <ProductList
            phoneList={phoneList}
            handleGetPhoneInfo={handleGetPhoneInfo}
          />
        </div>
        <ProductInfo phoneInfo={phoneInfo} />
      </div>
    </div>
  );
}
