import React from "react";
import ProductItem from "./ProductItem";

export default function ProductList({ phoneList, handleGetPhoneInfo }) {
  return (
    <div>
      <div className="row">
        {phoneList.map((item) => {
          return (
            <ProductItem
              key={item.maSP}
              phone={item}
              handleGetPhoneInfo={handleGetPhoneInfo}
            />
          );
        })}
      </div>
    </div>
  );
}
