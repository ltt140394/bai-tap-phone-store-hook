import React from "react";

export default function ProductItem({ phone, handleGetPhoneInfo }) {
  return (
    <div className="col-4">
      <div
        className="card w-100 p-3"
        style={{ height: "495px", borderRadius: "5px" }}
      >
        <img
          className="card-img-top"
          style={{ height: "320px" }}
          src={phone.hinhAnh}
          alt="Card image cap"
        />
        <div className="card-body p-0">
          <h2 className="card-title">{phone.tenSP}</h2>
          <p class="card-text text-danger font-italic font-weight-bold">
            {new Intl.NumberFormat("vn-VN").format(phone.giaBan)} VNĐ
          </p>
          <button
            onClick={() => {
              handleGetPhoneInfo(phone.maSP);
            }}
            className="btn btn-success"
            data-toggle="modal"
            data-target="#exampleModal"
          >
            Xem chi tiết
          </button>
        </div>
      </div>
    </div>
  );
}
